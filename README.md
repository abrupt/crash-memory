# ~/ABRÜPT/LILIANE GIRAUDON & MARA LALOUX & NICOLAS VERMEULIN/CRASH MEMORY/*

La [page de ce livre](https://abrupt.cc/giraudon-laloux-vermeulin/crash-memory/) sur le réseau.

## Sur le livre

Mais que deviennent les images lorsqu’elles ne sont plus reliées au monde des vivants, lorsqu’elles ont perdu leur narrativité, leur histoire, lorsqu’elles ne représentent plus rien pour les gens qui les regardent.

## Sur les autrices et l'auteur

### Liliane Giraudon

Née en 1946 Liliane Giraudon vit à Marseille. Entre ce qu’elle nomme « littérature de combat » et « littérature de poubelle », ses livres, publiés pour l’essentiel aux [éditions P.O.L](https://www.pol-editeur.com/index.php?spec=auteur&numauteur=86) dressent un spectre accidenté. À son travail de « revuiste » (Banana Split, Action Poétique, If, La gazette des jockeys camouflés) s’ajoute une pratique de la « lecture plateau » et du dessin. Livres d’artiste, expositions, ateliers de traduction, vidéo, théâtre, radio… [Et son site](https://www.lilianegiraudon.com/).

### Mara Laloux

Ethnographe. Vit à Bruxelles.

Proche des abeilles.

(Famille du carcharodon carcharias)

### Nicolas Vermeulin

Nicolas Vermeulin est un travailleur de l’art, manœuvre au 3x8 par choix d’une liberté, une allumette toujours prête à allumer une mèche. On dit de lui qu’il a perdu son temps, c’est vrai.

## Sur la licence

Cet [antilivre](https://abrupt.cc/antilivre/) est disponible sous licence Creative Commons Attribution – Pas d’Utilisation Commerciale – Partage dans les Mêmes Conditions 4.0 International (CC-BY-NC-SA 4.0).

## Etc.

Vous pouvez également découvrir notre [site](https://abrupt.cc) pour davantage d'informations sur notre démarche, notamment quant au [partage de nos textes](https://abrupt.cc/partage/).
