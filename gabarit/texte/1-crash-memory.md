```{=tex}
\clearpage
\vspace*{6\baselineskip}
\thispagestyle{empty}
\begin{nsright}
\itshape à Jean-Jacques Viton
\end{nsright}
\cleardoublepage
\begin{nscenter}
\includegraphics[width=0.5\textwidth]{./gabarit/images/0-debut.jpg}
\end{nscenter}
```
sommaire

```{=tex}
\clearpage
\begin{nscenter}
\includegraphics[width=0.5\textwidth]{./gabarit/images/0-sommaire.jpg}
\end{nscenter}
```
# 0x000000

```{=tex}
\begin{nscenter}
\includegraphics[width=0.5\textwidth]{./gabarit/images/1.jpg}
\end{nscenter}
```
Cinq. Peut-être six. De quoi parlent-ils ?

Nourri seulement de pain et d'eau, l'enfant avait craché la viande.

Séquestré. Seul et attaché. Longtemps. Les pieds sont nus. Les os déformés.

```{=tex}
\clearpage
\begin{nscenter}
\includegraphics[width=0.5\textwidth]{./gabarit/images/2.jpg}
\end{nscenter}
```
Dans tout son corps, les signes indubitables de cette longue catastrophe silencieuse. Sorti de la bouche, ce tas de phrases fracassées.

```{=tex}
\clearpage
\begin{nscenter}
\includegraphics[width=0.5\textwidth]{./gabarit/images/3.jpg}
\end{nscenter}
```
Comment articuler une naissance inconnue à une mort mystérieuse.

La double tentative d'assassinat. D'abord à Nuremberg, en plein jour et quasi publiquement. Puis Ansbach, un 14 décembre.

```{=tex}
\clearpage
\begin{nscenter}
\includegraphics[width=0.5\textwidth]{./gabarit/images/4.jpg}
\end{nscenter}
```
Celui qui a une fois flairé le sang au théâtre ne peut plus quitter le plateau.

```{=tex}
\clearpage
\begin{nscenter}
\includegraphics[width=0.5\textwidth]{./gabarit/images/5.jpg}
\end{nscenter}
```
Dans nos conversations, au prince maudit et au bâtard elle opposait sans cesse l'histoire de la fille-truie de Salzbourg.

```{=tex}
\clearpage
\begin{nscenter}
\includegraphics[width=0.5\textwidth]{./gabarit/images/6.jpg}
\end{nscenter}
```
Puis ce fut le tour des fillettes-louves capturées en Inde. Kamala et Amala. Nous étions en 1920.

```{=tex}
\clearpage
\begin{nscenter}
\includegraphics[width=0.5\textwidth]{./gabarit/images/7.jpg}
\end{nscenter}
```
"Il n'a pas un sou vaillant sur lui car je n'ai rien moi-même. Si vous ne le gardez pas avec vous, il vous faudra l'abattre ou l'enfermer et le pendre."

```{=tex}
\clearpage
\begin{nscenter}
\includegraphics[width=0.5\textwidth]{./gabarit/images/8.jpg}
\end{nscenter}
```
Au cou de Kaspar, un foulard de soie noire. Parfaite anagramme de l'expérience du crime.

# 0x000001

Ce sont des silences. Des absences. Des secrets oubliés.

On ne sait pas où vont ces cieux, ces paysages, ces rues, à qui sont ces maisons et ces jardins. On ne sait rien de ces corps, de ces visages, de ces allures.

```{=tex}
\begin{nscenter}
\includegraphics[width=0.5\textwidth]{./gabarit/images/9.jpg}
\end{nscenter}
```
Un couple autour d'un gâteau d'anniversaire. Une vieille femme assise devant un porche. Des garçons appuyés contre une voiture. Des enfants qui jouent dans l'herbe. Un homme en costume qui lève son verre. Des jeunes femmes sur un banc devant la mer. Il y a des sourires, des regards austères, des moments de joie, des moments d'ennui, des coiffures surprenantes, des vêtements d'avant, des familles, des groupes, des personnes seules, une fête de Noël, un mariage, une classe d'écoliers, une sieste au bord d'une rivière, un pique-nique sur un parking, la visite d'une cathédrale, une course cycliste, un déménagement.

```{=tex}
\begin{nscenter}
\includegraphics[width=0.5\textwidth]{./gabarit/images/10.jpg}
\end{nscenter}
```
On ne sait rien de ces images. On observe. On observe de plus près. On observe encore. Chacune fait partie d'une histoire sociale qui la dépasse. Chacune a une histoire singulière. Elles disent des vérités sur des mondes. Des vérités muettes. Des vérités perdues. Qui sont ces gens sur les photographies ?

Les gens des photographies anonymes, des photographies anciennes.

```{=tex}
\begin{nscenter}
\includegraphics[width=0.5\textwidth]{./gabarit/images/11.jpg}
\end{nscenter}
```
Considérer l'existence de ces absents d'un autre temps, d'un autre espace. Voir. Au-delà de ces tentatives en tout genre de figer le temps, au-delà des pauses orthodoxes, des instantanés manifestes, des nets, des flous, des maladresses, des formes maîtrisées. Saisir. Tenter d'attraper quelque chose. La perception de ce qui est familier et intime, ce qu'il y a de précieux et de vulnérable dans ces instants qui se sont tus, dans l'absence de ce qui est montré, dans ce qui demeure de ce qui a disparu.

```{=tex}
\begin{nscenter}
\includegraphics[width=0.5\textwidth]{./gabarit/images/12.jpg}
\end{nscenter}
```
Ces photographies sont des deuils. Ces photographies sont des ruines. Des memento mori. Des témoins poétiques et insolents des effacements et de la vélocité des existences. Ce sont des pièces à conviction. Ce sont des inventaires. Elles répertorient des fantômes. Des gens ont vécu, des gens ont disparu.

```{=tex}
\begin{nscenter}
\includegraphics[width=0.5\textwidth]{./gabarit/images/13.jpg}
\end{nscenter}
```
Restent ces quelques traces fragiles de ce qui a été au monde. Pour empêcher l'enfouissement. Pour que l'on croie sur images ce que nous ne verrons jamais, ce que nous ne verrons plus.

```{=tex}
\begin{nscenter}
\includegraphics[width=0.5\textwidth]{./gabarit/images/14.jpg}
\end{nscenter}
```
Que pouvons-nous dire de chaque personne, de chaque moment, de chaque matière qui a mérité l'attention d'une photographie et dont l'éphémère a été retenu ? De quoi notre œil peut-il prendre acte dans ce qui se montre et se dérobe à la fois ?

Ces images sont des histoires. Les regarder, c'est éprouver la beauté bouleversante de l'ordinaire et de l'universel ; l'expérience sensible de petites épiphanies quand on devine ce qu'elles semblent dire ; le trouble, devant des vies, des destinées, que nous découvrons à peine, de manière impudique, de manière minuscule, et dont nous n'apprendrons pourtant rien de plus.

```{=tex}
\begin{nscenter}
\includegraphics[width=0.5\textwidth]{./gabarit/images/15.jpg}
\end{nscenter}
```
Ces photographies ont eu une autre vie. Elles ont appartenu à quelqu'un avant d'être détachées de ce qui les protégeait : un album familial, un coffret précieux, un cadre, un tiroir fourre-tout, une boîte dans un grenier, un livre, un portefeuille. Par quel chemin, quelle distraction, quel hasard, quelle volonté ou quel besoin de s'affranchir se sont-elles retrouvées là, isolées, jetées dans des brocantes, des marchés aux puces, des magasins de seconde main ou des poubelles ? Selon quelle chronique familiale, quelle épreuve individuelle n'ont-elles pas pu être conservées avec soin ?

Il y a quelque chose de tragique à voir ces visages inconnus, ces scènes ordinaires, ces moments qui ponctuent des vies, surgir de clichés tout à coup exposés et dispersés en vrac.

```{=tex}
\begin{nscenter}
\includegraphics[width=0.5\textwidth]{./gabarit/images/16.jpg}
\end{nscenter}
```
Ce sont des objets perdus. Ils nous immergent dans le voyage mystérieux qu'ils ont fait, dont nous ne savons rien, sinon qu'il est question de perte et d'abandon. Des chaînes sociales interrompues dans les cycles du temps. Des accidents infaillibles dans la passation des choses. Des récits devenus fragments. Des charges affectives disparues.

Ces images ont signifié, elles ont véhiculé du sens. Des personnes leur ont confié une partie de leurs souvenirs, elles ont compté sur elles. Parce que les photographies privées ne sont pas exactement comme les autres objets. Elles abritent les corps. Elles gardent une trace de leurs présences, elles contredisent les disparitions. Et parce qu'elles capturent les empreintes affectives, la nostalgie, la mélancolie, la lumière, les blessures et l'obscur.

```{=tex}
\begin{nscenter}
\includegraphics[width=0.5\textwidth]{./gabarit/images/17.jpg}
\end{nscenter}
```
Ce que les humains font des photographies, font aux photographies, dit des choses sur ce qu'ils transmettent d'ordinaire ou d'important, des manières d'avoir prise, de donner prise, de perdre prise aussi, dans des formes d'expérience. C'est l'usage de ces images qu'il faut envisager. Ce sont des mondes qui leur donnent leurs fonctions. Des significations et des représentations qui s'animent dans le temps et dans l'espace. Qui ne peuvent pas s'établir à l'avance. Qui ne vont pas de soi.

C'est le contexte social qui compte. Ce que les photographies font faire. Ce que les humains en font. Entre les vivants. Avec les morts. Ici. Et ailleurs.

```{=tex}
\begin{nscenter}
\includegraphics[width=0.5\textwidth]{./gabarit/images/18.jpg}
\end{nscenter}
```
Les Roviana des îles Salomon pensent que les photographies personnelles sont des objets très singuliers, précieux et uniques. Et qu'on ne peut pas les reproduire. Parce qu'elles capturent l'âme d'une personne. Toucher la photographie d'une personne, c'est toucher son âme. Parler à sa photographie, c'est parler à son âme. Ils les utilisent pour communiquer avec des personnes éloignées. Parfois, les photographies capturent de mauvais esprits, sous la forme d'ombres floues. Elles sont alors très redoutées, et il faut s'assurer qu'elles ne soient jamais en contact avec les autres images. En Mongolie, on préfère brûler les photographies plutôt que risquer de les laisser dans des mains inconnues, comme nous le faisons ici. Elles sont utilisées avec beaucoup de précaution, car elles supposent des choses très délicates. Comme recevoir les bienfaits et les énergies d'une pratique chamanique à distance. Ou plus beau encore, être en connexion avec un défunt. À la mort d'une personne, un portrait d'elle est agrandi, colorisé, retouché et encadré. Et c'est cette image altérée qui devient comme un double du mort, qui permet de communiquer avec lui et de lui faire des offrandes. Le mort est transformé, au cours d'un processus rituel, il devient ancêtre, par le biais d'une photographie.

```{=tex}
\begin{nscenter}
\includegraphics[width=0.5\textwidth]{./gabarit/images/19.jpg}
\end{nscenter}
```
Partout où elles sont impliquées, les photographies fabriquent leur légitimité. Parce que les images ne sont jamais affaire du voir, mais affaire du dire. En dire, se dire, faire dire. À partir des images. Pour construire des narrations de soi et des autres, des discours, des mythes personnels et familiaux. Pour soutenir des normes et des pratiques. Assurer des origines, des identités, des choix sociaux. Par le jeu du rapport à la mémoire, aux zones d'ombre, aux secrets et à l'oubli. Et pour broder les fils, à la surface, ou plus profondément, de ce qui se transmet entre les générations, des continuités parmi les liaisons et les déliaisons, entre les vies individuelles et les histoires collectives, avant que ceux-ci ne disparaissent, eux aussi, de manière inéluctable.

Perdre ses photographies, c'est peut-être alors tout perdre.

```{=tex}
\begin{nscenter}
\includegraphics[width=0.5\textwidth]{./gabarit/images/20.jpg}
\end{nscenter}
```
Comme celles restées captives à Ishinomaki, enfouies dans les montagnes de décombres noyés sous la boue du tsunami. Des opérations de sauvetage ont porté secours à ces photographies de famille. Des brigades entières de bénévoles et d'associations japonaises ont collecté ces images, une à une. Quatre cent mille photographies nettoyées à la main, triées, séchées sur des fils à linge, restaurées et numérisées. Pour qu'elles soient reconnues par les survivants. Quand tout a été perdu, quand il ne reste plus que des scènes de dévastation, que les morts s'amoncellent, que maisons et immeubles ont été balayés, le soin porté à ces sauvetages dit tout le sens que les humains savent donner à leur fragilité. Secourir les images, pour secourir les mémoires. Faire le geste de les arracher à la boue. De les garder à part. Prendre appui sur ce qui importe que l'on s'y tienne. Comme promettre et permettre les continuités biographiques et que d'autres histoires de vie adviennent à partir d'elles.

```{=tex}
\begin{nscenter}
\includegraphics[width=0.5\textwidth]{./gabarit/images/21.jpg}
\end{nscenter}
```
Les photographies abandonnées dans le désordre des marchés et des vide-greniers sont détachées de ces trames sociales dans lesquelles elles étaient prises. Coupées de leur usage initial, elles restent indéchiffrables, dans l'effort qu'elles exigent. Parce qu'il n'y a plus personne, pour les dire, et pour les lire, dans les contextes intimes qui leur donnaient leur sens. Et parce que ces images ont perdu leur nom. Leur nom propre. Celui qui est de tous les rites et qui fait institutions. On ne peut plus les rattacher au nom d'un propriétaire. On ne peut plus les activer comme une mémoire proche de relations, de découvertes ou de retrouvailles liée à ce nom. Et cela agit sur nous comme une anomalie dans l'ordre social habituel, où les choses du monde sont identifiées et liées à des entités sociales.

```{=tex}
\begin{nscenter}
\includegraphics[width=0.5\textwidth]{./gabarit/images/22.jpg}
\end{nscenter}
```
Ces photographies ne disent plus rien à personne. Elles ne disent plus rien. Mais elles font dire. Elles font encore dire. Elles invoquent. Comme ce qui fascine. Comme ce qui creuse le désir et le malaise en même temps. Quelque chose de cet ordre se joue en leur présence. Elles interpellent. Elles posent question. Et c'est en faisant dire, qu'elles résistent. C'est en faisant parler, qu'elles s'agitent et se débattent sur les lignes ténues qui les mènent parfois à la récupération, à de nouvelles utilisations et appropriations individuelles et collectives. Quand elles sont intégrées à des collections et qu'elles servent d'archives et de témoins de l'histoire de la photographie et de l'art. Quand elles font écrire, en inspirant des fictions, des personnages racontés par-dessus les gens et les lieux qu'elles montrent. Quand elles sont numérisées et partagées dans des mondes virtuels pour être réutilisées par le plus grand nombre. Quand elles servent de matériau à des artistes, des cinéastes, des poètes qui en font la matière première de leur démarche et les font sortir de l'indicible.

C'est de ces manières que s'invente, quelquefois, et encore, la vie sociale des photographies perdues. En prenant place dans d'autres univers, avec d'autres statuts.

```{=tex}
\begin{nscenter}
\includegraphics[width=0.5\textwidth]{./gabarit/images/23.jpg}
\end{nscenter}
```
Ces images attestent de ce qui se noue et s'est noué, à double sens, entre elles et les mondes humains dont elles sont issues. Parce qu'elles ont été investies, et qu'en retour, elles ont investi. Comme deux matières qui se nourrissent, se contraignent, se libèrent, se font et se défont dans un même mouvement, continu et sensible. L'enjeu est de taille, il parle de ce qui se joue entre les êtres et les choses. Les photographies coupées de leurs liens sont des photographies perdues, vouées à la destruction ou en attente d'autres utilisations ; et les humains privés de leurs images personnelles sont dépossédés ou libérés d'une part d'eux-mêmes, de manières de penser et de vivre, qu'ils ont élaboré à partir d'elles.

```{=tex}
\begin{nscenter}
\includegraphics[width=0.5\textwidth]{./gabarit/images/24.jpg}
\end{nscenter}
```
Il se dit là, au creux de ces photographies qui nous sont si personnelles, des mondes, et des rapports au monde.

Les sociétés humaines accordent des places aux images. Elles s'en servent pour mener à bien des tâches. Des tâches ordinaires. Des tâches particulières. Des tâches délicates. Et ces images ont leur propre vie, leur puissance agissante et leur trajectoire. À travers des espaces et des temporalités. Elles sont censées contenir et faire raconter des histoires, faire des histoires même, car elles portent l'empreinte de logiques sociales et qu'elles sont les preuves des liens complexes que les humains entretiennent entre eux, là où ils sont.

```{=tex}
\begin{nscenter}
\includegraphics[width=0.5\textwidth]{./gabarit/images/25.jpg}
\end{nscenter}
```
Hervé Guibert écrivait dans son livre *L'image fantôme* : "Voilà la preuve que les photos ne sont pas innocentes, qu'elles ne sont pas lettres mortes, objets inanimés, embaumés par le fixateur. Voilà la preuve qu'elles agissent et qu'elles trahissent ce qui se cache derrière la peau."

```{=tex}
\begin{nscenter}
\includegraphics[width=0.5\textwidth]{./gabarit/images/26.jpg}
\end{nscenter}
```
Voilà la preuve que les images sont ces objets habités, rêvés, pensés dans les relations à soi et aux autres, qui incorporent du sens et qui nous constituent en tant que sujet. Présentes là où se construisent les liens. Et là où ils se déconstruisent. Là où le social commence et se donne à voir. Et là où il disparaît. Témoignant, de manière émouvante, entre l'évidence et le mystère, comment les destinées humaines s'inventent et s'imbriquent aux destinées de ces photographies.

```{=tex}
\clearpage
\begin{nscenter}
\includegraphics[width=0.5\textwidth]{./gabarit/images/0-fin.jpg}
\end{nscenter}
```
